# from speech_recog import listen
from speech_functionality import speech_recog, talk


########### Commands working so far ###########
# (hey brian)
# 'play some of my music' -> will play music by random from the given folder specified in media_functionality/music.py
# 'play some "X"' -> will search for the given keyword(s) on youtube and play found music by random
# 'pause' -> will pause local music
# 'resume' -> will resume local music
# 'what time is it' -> will tell the user the current time
# Multiple commands only work with local music but still tricky
# project is in a very early stage of development, see gitlab for planned/upcoming features


def welcome():
    talk.init(True)
    # talk.say_welcome_message()

    speech_recog.create_listening_thread()
    # import utils
    # print(utils.lookup_time())
    # from subprocess import Popen, PIPE
    # process = Popen('mpsyt', stdin=PIPE, stdout=PIPE)
    # while True:
    #     value = input("Input some text or number")
    #     process.stdin.write(value)



def change_input(process, str):
    process.stdin.write(str)


welcome()