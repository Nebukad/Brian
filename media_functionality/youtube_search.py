from bs4 import BeautifulSoup
import bs4, requests


def scrape_youtube(search, is_specific):
    search_term = '+'.join(search)
    text = requests.get('https://www.youtube.com/results?search_query='+search_term).text
    soup = BeautifulSoup(text, 'html.parser')

    div = [ d for d in soup.find_all('div') if d.has_attr('class') and 'yt-lockup-dismissable' in d['class'] ]
    urls = []
    for d in div:
        if urls.__len__() >= 10:
            break
        a0 = d.find_all('a')[0]
        a0 = [ x for x in d.find_all('a') if x.has_attr('title')][0]
        title = a0['title']
        url = 'https://www.youtube.com/'+a0['href']
        if is_specific:
            if exact_match(search, title):
                urls.append(url)
                print(str(urls))
                return urls
        else:
            if any_match(search, title):
                urls.append(url)
    return urls


def config_search(search, func_one, func_two):
    func = lambda s, f: f(s[:1]) + s[1:] if s else ''
    both_config = func(str.split(search, ' ')[0], func_one) + ' ' + func(str.split(search, ' ')[1], func_two)
    return both_config


def any_match(search, found):
    # Not necessary, way to complicated, but nice to know
    # possibilities = []
    # if str.split(search, ' ').__len__() > 1:
    #     possibilities = [str.upper(search),
    #                      config_search(search, str.lower, str.upper),
    #                      config_search(search, str.upper, str.upper),
    #                      config_search(search, str.upper, str.lower)]
    # else:
    #     possibilities = [str.upper(search[:1]) + search[1:],
    #                      str.lower(search[:1]) + search[1:]]

    search = str.lower(search)
    found = str.lower(found)
    possibilities = []

    if str.split(search, ' ').__len__() > 1:
        possibilities.extend(str.split(search, ' '))
    else:
        possibilities.append(search)

    possibilities = filter_list(possibilities)
    is_match = any(str.__contains__(found, item) for item in possibilities)
    return is_match


def exact_match(search, found):
    search = str.lower(search)
    found = str.split(str.lower(found), ' ')
    possibilities = []

    if str.split(search, ' ').__len__() > 1:
        possibilities.extend(str.split(search, ' '))
    else:
        print('Not multiple words, adding: ' + search)
        possibilities.__add__(search)

    possibilities = filter_list(possibilities)
    isSubset = set(possibilities).issubset(found)
    return isSubset


def filter_list(lst):
    while list.__contains__(lst, ''):
        list.remove(lst, '')
    return lst