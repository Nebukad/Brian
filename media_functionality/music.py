import subprocess
import threading
from os import listdir
from random import choice
import pygame
from speech_functionality import talk
from media_functionality import youtube_search
import spotipy

# config - local music
chunk = 1024
local_music_path = '/home/numelen/Music/sync/'

# data
local_music_files = []


class MusicThread(threading.Thread):
    def __init__(self, location, search):
        super(MusicThread, self).__init__()
        self._stop_event = threading.Event()

        self.location = location
        self.search = search
        if location == 'local':
            global local_music_files
            local_music_files = listdir(local_music_path)
            self.file = choice(local_music_files)
            self.mixer = pygame.mixer
            self.mixer.init(48000, -16, 1, 1024)
            self.sound = self.mixer.Sound(local_music_path + self.file)
        elif location == 'spotify':
            pass
        elif location == 'youtube':
            self.vlc_stream = None

    def run(self):
        if self.location == 'local':
            self.run_local()
        elif self.location == 'spotify':
            self.run_spotify()
        else:
            print('running youtube')
            self.run_youtube()

    def run_local(self):
        self.sound.play()

    def run_youtube(self):
        url = str(self.location)
        print('playing: ' + str(self.location))
        # this seems to download the entire video before streaming -> not what I am looking for
        # subprocess.check_output('youtube-dl -o - ' + url + ' | mplayer -', shell=True)
        print('Setting vlc_stream')
        self.vlc_stream = subprocess.check_call('exec vlc --novideo ' + url, shell=True)
        # browser.Browser().visit(url)
        # while str.__contains__(self.search, ' '):
        #     self.search = str.replace(self.search, ' ', '+')
        # command = 'https://www.youtube.com/results?search_query=' + self.search
        # self.vlc_stream = subprocess.check_output('lynx -dump ' + command + ' | egrep -o "http.*watch.*" | vlc -')

    def run_spotify(self):
        sp = spotipy.Spotify()
        results = sp.search(q=self.search, limit=20)
        results['tracks']['items'][0]

    def pause(self):
        if self.location == 'local':
            self.mixer.pause()

    def resume(self):
        if self.location == 'local':
            self.mixer.unpause()

    def stop(self):
        if self.location == 'local':
            self.mixer.stop()
        else:
            self.vlc_stream.kill()
        print('ending music thread')
        self._stop_event.set()

    def lower_volume(self):
        if self.location == 'local':
            print('lowering')
            current_volume = self.mixer.music.get_volume()
            # self.mixer.music.set_volume(current_volume * 0.75)
            self.mixer.music.set_volume(0.5)

    def raise_volume(self):
        if self.location == 'local':
            current_volume = self.mixer.music.get_volume()
            self.mixer.music.set_volume(current_volume * 1.25)

    def set_volume(self, percentage):
        if self.location == 'local':
            if 100 > percentage > 0:
                self.mixer.music.set_volume(percentage)

    def stopped(self):
        return self._stop_event.is_set()


def play_mix_from_youtube(speech):
    urls = youtube_search.scrape_youtube(speech, False)
    if urls.__len__() == 0:
        talk.speak('Didnt find any.')
        return
    talk.say_ok()
    talk.speak('shuffling ' + str(speech))
    global music_thread
    song = choice(urls)
    music_thread = MusicThread('youtube', song)
    music_thread.start()


def play_song_from_youtube(speech, name):
    urls = youtube_search.scrape_youtube(speech, True)
    if urls.__len__() == 0:
        talk.speak('didnt find ' + str(name))
        return
    talk.say_ok()
    talk.speak('playing ' + str(name))
    global music_thread
    song = urls[0]
    music_thread = MusicThread(song)
    music_thread.start()


def play_random_local_music():
    talk.say_ok()
    talk.speak('shuffling music library.')
    global music_thread
    music_thread = MusicThread('local', '')
    # main.set_main_thread(music_thread)
    music_thread.start()


def play_music_from_spotify(speech):
    talk.say_ok()
    talk.speak('shuffling ' + speech + ' from spotify.')
    global music_thread
    music_thread = MusicThread('spotify', speech)
    # main.set_main_thread(music_thread)
    music_thread.start()


def stop_music():
    global music_thread
    music_thread.stop()


def pause_music():
    global music_thread
    music_thread.pause()


def resume_music():
    global music_thread
    music_thread.resume()


def raise_volume():
    global music_thread
    music_thread.raise_volume()


def lower_volume():
    global music_thread
    music_thread.lower_volume()


def set_volume(percentage):
    global music_thread
    music_thread.set_volume(percentage)