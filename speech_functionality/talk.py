import pyttsx3
from miscellaneous import utils

is_silent = False


def init(is_talk_silent):
    global engine
    engine = pyttsx3.init()
    global is_silent
    is_silent = is_talk_silent


def speak(message):
    if is_silent:
        print(message)
    global engine
    output = engine.say(message)
    engine.runAndWait()


def say_time():
    unformatted_time = utils.lookup_time()
    time_sentence = 'It is ' + unformatted_time[0] + ' and ' + unformatted_time[1] + ' minutes.'
    # music.lower_volume()
    speak(time_sentence)
    # music.raise_volume()
    return unformatted_time


def say_ok():
    speak('Ok.')


def say_welcome_message():
    time_of_day = ''
    time = int(utils.lookup_time()[0])
    if time < 12:
        time_of_day = 'morning'
    elif 12 < time < 18:
        time_of_day = 'day'
    else:
        time_of_day = 'evening'

    speak('Good ' + time_of_day + ' Pascal.')