import speech_recognition as sr
from speech_functionality import talk
from media_functionality import music
import automation_functionality.system
import threading
from time import sleep


# Remember replace with request from list
def parse_speech(speech):
    print('Understood: ' + speech)
    if str.startswith(speech, 'hey brian'):
        #global listening_thread
        #listening_thread.deafen()
        print('listening')
        speech = str.replace(speech, 'hey brian ', '')
        # restart_listening_thread()
        if speech == 'what time is it':
            talk.say_time()
        elif str.__contains__(speech, 'play some') and str.__contains__(speech, 'my music'):
            music.play_random_local_music()
        elif str.__contains__(speech, 'play some'):
            speech = str.replace(speech, 'play some ', '')
            # music.play_mix_from_youtube(speech)
            music.play_mix_from_youtube(speech)
        elif str.__contains__(speech, 'play') and str.__contains__(speech, 'by'):
            speech = speech.replace('play', '')
            name = speech
            speech = speech.replace('by', '')
            music.play_song_from_youtube(speech, name)
        elif speech == 'stop':
            music.stop_music()
        elif speech == 'pause':
            music.pause_music()
        elif speech == 'resume':
            music.resume_music()
        elif speech == 'lower volume':
            music.lower_volume()
        elif speech == 'raise volume':
            music.raise_volume()
        elif str.__contains__(speech, 'set volume to') and str.endswith(speech, 'percent'):
            speech = str.replace(speech, ' percent', '')
            speech = str.split(speech, ' ')
            length = speech.__len__() - 1
            print(speech[length])
            music.set_volume(int(speech[length]))
        elif str.__contains__(speech, 'update system'):
            automation_functionality.system.update_system()
        elif str.__contains__(speech, 'sync music'):
            automation_functionality.system.sync_music()
        elif is_speech_contains(speech, 'goodbye'):
            talk.speak('Goodbye.')
            close_listening_thread()
            exit(0)
        else:
            print("I didn't understand that")
            talk.speak("I didn't understand that")
    # restart_listening_thread()


class ListenThread(threading.Thread):
    def __init__(self):
        super(ListenThread, self).__init__()
        self._stop_event = threading.Event()
        self._deaf_event = threading.Event()

    def run(self):
        #   Change for silent / listening mode
        # listen_keyboard()
        while not self.stopped():
            if self.deaf():
                print('sleeping')
                sleep(5)
                print('waking up')
                self._deaf_event.clear()
            listen()

    def deafen(self, time):
        self._deaf_event.set()

    def deaf(self):
        return self._deaf_event.is_set()

    def stop(self):
        self._stop_event.set()

    def stopped(self):
        return self._stop_event.is_set()


def create_listening_thread():
    global listening_thread
    listening_thread = ListenThread()
    listening_thread.start()


def close_listening_thread():
    global listening_thread
    listening_thread.stop()


def listen():
    # obtain audio from the microphone
    r = sr.Recognizer()
    with sr.Microphone() as source:
        print("Say something!")
        audio = r.listen(source)
        try:
            ## Change for different voice recognizer, google seems to be far more dependable -> going to change to own voice recognition
            # spoken = r.recognize_sphinx(audio)
            spoken = r.recognize_google(audio)
            spoken = str.lower(spoken)
            parse_speech(spoken)
        except sr.UnknownValueError:
            print("Could not understand audio")
        except sr.RequestError as e:
            print("Error; {0}".format(e))


def listen_keyboard():
    print('Write something')
    parse_speech(input())


def is_speech_contains(speech, part):
    return str.__contains__(speech, part)

