import subprocess
import speech_functionality.talk as talk
import threading

# config
scripts_path = '/home/numelen/.scripts/auto/'


class SystemThread(threading.Thread):
    def __init__(self, command, msg_before, msg_after):
        super(SystemThread, self).__init__()
        self.command = command
        self.msg_before = msg_before
        self.msg_after = msg_after

    def run(self):
        talk.say_ok()
        talk.speak(self.msg_before)
        subprocess.check_output(self.command, shell=True)
        talk.speak(self.msg_after)


def update_system():
    update_system_thread = SystemThread(scripts_path + 'pacaur.exp', 'Updating system', 'System updated.')
    update_system_thread.start()


def sync_music():
    update_system_thread = SystemThread('python ' + scripts_path + 'sync_music.py', 'Syncing music library', 'Music library synced.')
    update_system_thread.start()